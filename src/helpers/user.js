import firebase from 'react-native-firebase';
import moment from 'moment';
import userLogin from '../redux/userActions';
import store from '../redux/store';


import { getCurrentWeek } from './DateGetter'

const userSchema =
{
  address: null,
  restrictions: null,
  dateOfBirth: null,
  displayName: null,
  phone: null,
  email: null,
  weeks: null,
  uid: null,
  catering: null,
  base: null
};

const dishesUserSchema =
{
  Did: null,
  name: null,
  picture: null,
  rated: false,
};

// week starts on moday in "moment"
/*
function emptyWeekSchema(week) {
  const year = week.substring(0, 4);
  const weekNum = week.substring(5, 7);
  const sunday = moment(year).add(weekNum - 1, 'weeks').add(-1, 'days').format('YYYY-MM-DD');
  const monday = moment(year).add(weekNum - 1, 'weeks').format('YYYY-MM-DD');
  const tuesday = moment(year).add(weekNum - 1, 'weeks').add(1, 'days').format('YYYY-MM-DD');
  const wednesday = moment(year).add(weekNum - 1, 'weeks').add(2, 'days').format('YYYY-MM-DD');
  const thursday = moment(year).add(weekNum - 1, 'weeks').add(3, 'days').format('YYYY-MM-DD');
  console.log('week in empty week schema', weekNum);
  console.log('year', year);
  const emptyWeek = {
    [sunday]: [],
    [monday]: [],
    [tuesday]: [],
    [wednesday]: [],
    [thursday]: [],
  };

  return emptyWeek;
}; // emptyWeekSchema
*/


// This function returns a promise. in UserActions there is  .then...
function updateAddress(uid, addressObject) {
  const userRef = firebase.firestore().collection('Users').doc(uid);
  return userRef.update({
    address: addressObject,
  })
    .catch((error) => {
      // The document probably doesn't exist.
      console.error('"Error updating document: "', error);
    });
}

function updateUserField(user, data, fieldName) {
  const newUser = user;
  console.log(user);
  newUser[fieldName] = data;
  const userRef = firebase.firestore().collection('Users').doc(newUser.uid);
  return userRef.update(newUser)
    .catch((error) => {
      // The document probably doesn't exist.
      console.error('"Error updating document: "', error);
    });
}

// This function returns a promise
function getCurrentFirestoreUser() {
  const user = firebase.auth().currentUser;
  const Users = firebase.firestore().collection('Users');
  return Users.doc(user ? user.uid : null).get();
}


function getCurrentFirestoreUserDishes(user, week) {
  console.log('get current firstore user dishes', user);
  const currentWeek = week; // changed 10.5 manualy
  const userWeeks = firebase.firestore()
    .collection('Users')
    .doc(user.uid)
    .collection('Weeks');

  console.log("week in getCurrentFirestoreUserDishes call:", currentWeek);
  return userWeeks.doc(currentWeek).get()
    .then((docSnapshot) => {
      if (docSnapshot.exists) {
        console.log('docsnapshot:', docSnapshot);
        return userWeeks.doc(currentWeek).get();
      }
      //const newWeek = emptyWeekSchema(currentWeek);
      //userWeeks.doc(currentWeek).set(newWeek);
      return userWeeks.doc(currentWeek).get();
    }); //.then
} // function getCurrentFirestoreUserDishes



function addUserToFirebase(user, name) {
  const Users = firebase.firestore().collection('Users');
  const { uid, email, phoneNumber, photoURL } = user;
  return Users.doc(uid).get()
    .then((docSnapshot) => {
      if (docSnapshot.exists) {
        return;
      }
      const newUser = userSchema;
      newUser.uid = uid;
      newUser.displayName = name;
      newUser.email = email;
      newUser.phoneNumber = phoneNumber;
      newUser.photoURL = photoURL;
      Users.doc(uid).set(newUser);
      // this updates redux store with the new registred user
      store.dispatch(userLogin(newUser));
    });
}

function addEmailToUserInFirebase(user, email) {
  const Users = firebase.firestore().collection('Users');
  const { uid } = user;
  Users.doc(uid).update({ email });
}

export { userSchema,
  addUserToFirebase,
  addEmailToUserInFirebase,
  getCurrentFirestoreUser,
  updateAddress,
  updateUserField,
  getCurrentFirestoreUserDishes };
