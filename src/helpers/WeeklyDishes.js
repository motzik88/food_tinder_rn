import firebase from 'react-native-firebase';
import currentWeekNumber from 'current-week-number';


// This function returns a promise of all the days of this week
function getDaysOfWeek(user) {
  if (!user) { return; }
  // get base and catering from the user
  const { base, catering } = user;
  // make a reference to the weeks collection   <------- change 'EXAMPLE' to 'catering' and 'Tzrifin' to 'base'!!!!!!!!
  const weeksRef = firebase.firestore().collection('Caterings').doc('EXAMPLE')
    .collection('Bases').doc('Tzrifin').collection('Weeks');
  // get current week number
  const today = new Date();
  const weekNum = currentWeekNumber(today);
  const currWeekNum = today.getFullYear().toString() + "_" + weekNum.toString();
  // take the daily dishes from each day and add them to an array <----- change '2018_18' for currWeekNum
  const daysRef = weeksRef.doc('2018_18').collection('Days');

  return daysRef.get();
}


/*
  this function recives as argument a day document
  creates a reference to that day
  runs a snapshotQuery to get the dishes that are in the menu of that day
  insert each dish to dailyDishes array
  returns an array with all the dishes of that day
*/
function insertDailyMenuToArray(day) {
  const dailyDishes = [];
  const tempDayRef = day.ref.parent;
  tempDayRef.doc(day.id).collection('Daily Dishes').onSnapshot((snapshotQuery) => {
    snapshotQuery.forEach((dish) => {
      // adding into array key = id, value = dish document data
      dailyDishes[dish.id] = dish.data();
    });
  });
  return dailyDishes;
}

export { getDaysOfWeek, insertDailyMenuToArray };
