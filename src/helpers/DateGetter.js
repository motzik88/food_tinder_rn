import React, { Component } from 'react';
import currentWeekNumber from 'current-week-number';

const today = new Date();

// returns date in yyyy-mm-dd format
function getCurrentDate() {
  const date= today.getFullYear() +"-"+ parseInt(today.getMonth()+1) +"-"+ today.getDate();
  return date;
}


// returns week in ww_yyyy format. insert number for the number of weeks you want to reduce
function getCurrentWeek(last) {
  const weekNum = (!last) ? currentWeekNumber(today) : parseInt(currentWeekNumber(today) - last);
  const currWeek = today.getFullYear().toString() + "_" + weekNum.toString();
  return currWeek;
}

export { getCurrentDate, getCurrentWeek };
