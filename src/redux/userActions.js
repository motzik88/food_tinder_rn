import firebase from 'react-native-firebase';


import { getCurrentFirestoreUser, addUserToFirebase, getCurrentFirestoreUserDishes } from '../helpers/user';
import store from './store';
import { weeklyMenu } from './WeeklyDishesActions';
import { getCurrentWeek } from '../helpers/DateGetter';


export function userLogin(callback) {
  return (dispatch) => {
    // The "GET" Request
    getCurrentFirestoreUser()
      .then((docSnapshot) => {
        // In case the auth user exist but it is not exists in the firestore database(rare - mostly for dev)
        if (!docSnapshot.exists) {
          const authUser = firebase.auth().currentUser;
          addUserToFirebase(authUser, authUser.displayName)
            .then(() => { userLogin(); });
          return;
        }
        // After the request is complete, we dispatch the action, and call the callback
        dispatch({ type: 'login', payload: (docSnapshot.data()) });
        store.dispatch(weeklyMenu(docSnapshot.data()));
        callback ? callback() : null;
      });
  };
}

export function updateUser(user) {
  return ({
    type: 'updateUser',
    payload: user,
  });
}

export const userLogout = () => ({
  type: 'logout',
});

// at first called this function after userLogin() in App.js. [dispatch. then (() = > dispatch..)]
export function userGetDishes(user, week, callback) {
  console.log('user get dishes');
  return (dispatch) => {
    getCurrentFirestoreUserDishes(user, week)
      .then((docSnapshot) => {
        dispatch((week === '2018_18') ? // change 10.5 manualy
          { type: 'getDishes', payload: docSnapshot.data() } :
          { type: 'getDishes1', payload: docSnapshot.data() });
        if (callback) callback();
      });
  };
}
