

export default function userReducer(user = null, action) {
  switch (action.type) {
    case 'login':
      return action.payload;

    case 'updateUser':
      return { ...action.payload };

    case 'logout':
      return {
        loading: true,
      };

    case 'getDishes':
      let newUser = {...user};
      newUser.currentWeek = action.payload;
      return newUser;

    case 'getDishes1':
      newUser = {...user}
      newUser.lastWeek = action.payload
      return newUser;

    default:
      return user;
  }
}
