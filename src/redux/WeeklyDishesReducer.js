

export default function WeeklyDishesReducer(WeeklyDishes = [], action) {
  switch (action.type) {
    case 'getWeeklyDishes':
      return action.payload;

    default:
      return WeeklyDishes;
  }
}
