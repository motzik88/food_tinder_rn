import firebase from 'react-native-firebase';

export function getRestrictions(callback) {
  return (dispatch) => {
    const Restrictions = firebase.firestore().collection('Restrictions');
    Restrictions.get()
      .then((docSnapshot) => {
        dispatch({ type: 'getRestrictions', payload: (docSnapshot.docs) });
        if (callback) callback();
      });
  };
}

export const userLogout = () => ({
  type: 'logout',
});
