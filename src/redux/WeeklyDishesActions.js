import { getDaysOfWeek, insertDailyMenuToArray } from '../helpers/WeeklyDishes';

let menu = [];

export function weeklyMenu(user) {
  /*
    this function is called with the current user as parameter
    the function gets the menu of all week in an array and dispatch
    the action with the menu
  */
  return (dispatch) => {
    try {
      // first empty the array menu
      (menu.length > 0) ? menu = [] : menu;
      // getDaysOfWeek returns a promise with the days of the relevant week
      getDaysOfWeek(user)
        .then((days) => {
          // each day is send to insertDailyMenuToArray and from that function returns
          // an array with all the dishes of that days
          days.forEach((doc) => {
            const dailyDishes = insertDailyMenuToArray(doc);
            // each day has a dailyDishes array and its inserted in the menu array
            menu.push({
              day: dailyDishes,
            });
          });
          dispatch({ type: 'getWeeklyDishes', payload: menu });
        })
        .catch((error) => {
          console.log(`error adding Firestore document to menu in action = ${error}`);
        });
    } catch (error) { console.log('error in WeeklyDishesActions', error); }
  };
}
