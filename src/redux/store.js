/**
 * @flow
 *
 * This file configures the Redux store
 *
 * To keep things simple, and because Firebase already acts as a state management solution,
 * we are only using Redux for the following:
 *
 * 1) `redux-form` integration
 * 2) Basic UI state, i.e. the loading modal
 */
import ReduxThunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { reducer as formReducer } from 'redux-form';

import uiReducer from '../ui/redux/uiReducer';
import userReducer from '../redux/userReducer';
import restrictionsReducer from '../redux/RestrictionsReducer';
import WeeklyDishesReducer from './WeeklyDishesReducer';

const rootReducer = combineReducers({
  form: formReducer,
  ui: uiReducer,
  firestoreUser: userReducer,
  restrictions: restrictionsReducer,
  menu: WeeklyDishesReducer,
});

const store = createStore(rootReducer, {}, applyMiddleware(ReduxThunk));
export default store;
