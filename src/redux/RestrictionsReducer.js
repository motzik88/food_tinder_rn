

export default function restrictionReducer(restrictions = null, action) {
  switch (action.type) {
    case 'getRestrictions':
      return action.payload;


    default:
      return restrictions;
  }
}
