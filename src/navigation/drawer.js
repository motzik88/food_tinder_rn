import { DrawerItems, SafeAreaView } from 'react-navigation';
import { Image, StyleSheet, ScrollView, Text, View, TouchableHighlight } from 'react-native';
import React from 'react';
import firebase from 'react-native-firebase';

import Icon from '../ui/components/Icon';
import * as Theme from '../theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: Theme.PRIMARY,
    padding: 5,
    shadowColor: 'red',
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 0.8,
    shadowRadius: 2,

  },
  photo: {
    alignSelf: 'center',
    borderRadius: 50,
    marginBottom: 8,
    marginTop: 16,
  },
  photoImage: {
    height: 100,
    width: 100,
  },
  photoIcon: {
    fontSize: 100,
    height: 100,
  },
  nameText: {
    alignSelf: 'center',
    fontSize: 18,
  },
});


class Drawer extends React.Component {
  constructor(props, context) {
    super(props, context);
    const user = firebase.auth().currentUser;

    // Set the default state of the component
    this.state = {
      user,
    };
  }


  render() {
    const { navigation } = this.props;
    const { user } = this.state;

    // This will be the function that called when the profile header is pressed.
    // If we are in profile screen we need just to close the drawer instead of navigating
    // And if we are in a different screen, we need to navigate to Profile.
    let profileViewButton = () => this.props.navigation.navigate('DrawerClose');
    if (navigation.state.routes[navigation.state.index].routeName !== 'Profile') {
      profileViewButton = () => this.props.navigation.navigate('Profile');
    }

    return (
      <ScrollView>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
          <TouchableHighlight onPress={profileViewButton}>
            <View style={styles.header}>
              {user.photoURL
                ? (
                  <Image
                    source={{ uri: user.photoURL }}
                    style={[styles.photo, styles.photoImage]}
                  />
                ) : (
                  <Icon
                    active
                    name="md-person"
                    style={[styles.photo, styles.photoIcon]}
                  />
                )}
              <Text style={styles.nameText}>{user.displayName}</Text>
            </View>
          </TouchableHighlight>
          <DrawerItems {...this.props} />
        </SafeAreaView>
      </ScrollView>
    );
  }
}


export default (Drawer);
