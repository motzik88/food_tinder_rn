/**
 * @flow
 *
 * The Logged In Home screen is a simple screen indicating that the user has logged in.
 */
import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';

import Screen from '../../ui/components/Screen';
import PMLogo from '../../../assets/PMLogo.png';

// test for render the feed in the application
import List from '../../ui/components/list/List';
import ListHeader from '../../ui/components/list/ListHeader';
import ListItem from '../../ui/components/list/ListItem';
import Icon from '../../ui/components/Icon';

// this imports are for rendering the feed, its a test

const styles = StyleSheet.create({
  drawerIcon: {
    fontSize: 32,
    marginLeft: 10,
  },
  itemIcon: {
    fontSize: 32,
  },
  image: {
    height: 350,
    width: 300,
  },
  welcome: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    padding: 24,
  },
  PMText: {
    fontSize: 28,
    marginTop: 24,
    textAlign: 'center',
  },
  secText: {
    fontSize: 20,
    marginTop: 5,
    textAlign: 'center',
  },
});

class Home extends React.Component<*> {
  // Set the navigation options for `react-navigation`
  static navigationOptions = ({ navigation }) => ({
    headerLeft: <Icon name="md-menu" style={styles.drawerIcon} onPress={() => navigation.navigate('DrawerToggle')} />,
    headerTitle: 'בית',
    drawerIcon: <Icon name="md-home" style={styles.itemIcon} />,

  });


  render() {
    return (
      <Screen>
        <View style={styles.welcome}>
          <Image source={PMLogo} style={styles.image} />
          <Text style={styles.PMText}>
            Personal Menu
          </Text>
          <Text style={styles.secText}>
            It's easy to decide what to eat!
          </Text>
        </View>
      </Screen>
    );
  }
}

const stateToProps = (state) => {
  return { firestoreUser: state.firestoreUser };
};

export default connect(stateToProps)(Home);
