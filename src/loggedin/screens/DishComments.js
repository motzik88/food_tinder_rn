import firebase from 'react-native-firebase';
import React from 'react';
import { FlatList } from 'react-native';
import DishDetailHeader from '../../ui/components/DishDetailHeader';
import Screen from '../../ui/components/Screen';
import Comment from './../../ui/components/Comment';

class DishComments extends React.Component<*> {
  // Set the navigation options for `react-navigation`

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return {
      headerTitle: <DishDetailHeader
        dish={params.dish}
        onPress={() => { navigation.goBack(); }}
                   />,
      headerStyle: { height: 300 },
      headerLeft: null,
    };
  };

  constructor(props) {
    super(props);
    const { dish } = this.props.navigation.state.params;
    this.commentsRef = firebase.firestore().collection('Caterings').doc('EXAMPLE')
      .collection('Dishes')
      .doc('Shnitzel')
      .collection('Reviews')
      .orderBy('Date', 'desc');
    this.state = ({
      stateComments: [],
    });
  }

  componentWillMount() {
    this.unsubscribe = this.commentsRef.onSnapshot((querySnapshot) => {
      const comments = [];
      querySnapshot.forEach((doc) => {
        comments.push({
          comment: doc.data(),
        });
      });
      this.setState({
        stateComments: comments,
      });
    });
  }

  render() {
    return (
      <Screen>
        <FlatList
          data={this.state.stateComments}
          renderItem={({ item }) =>
            (<Comment
              commentUserImage={item.comment.UserPhoto}
              rating={item.comment.Rating}
              comment={item.comment.Review}
              commentUserName={item.comment.UserName}
              commentDate={item.comment.Date}
             />)}
        />
      </Screen>
    );
  }
}

export default DishComments;
