import React from 'react';
import { Text, ScrollView, View, Image } from 'react-native';
import DishDetailHeader from '../../ui/components/DishDetailHeader';
import Screen from '../../ui/components/Screen';
import renderIf from './../../loggedin/functions/renderIf';
import Icon from './../../ui/components/Icon';


const styles = {
  nameContainerStyle: {
    // backgroundColor: 'rgba(45, 50, 37, 0.7)',
    flexDirection: 'row',
    // justifyContent: 'space-between',
  },
  imageStyle: {
    alignSelf: 'center',
    padding: 5,
    paddingRight: 5,
    height: 35,
    width: 35,
    // fontSize: 30,
    // color: 'rgb(255, 255, 255)',
  },
  mainTextStyle: {
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 8,
    marginRight: 8,
  },
  secondTextStyle: {
    fontSize: 22,
    marginLeft: 8,
    marginRight: 8,
  },
  iconStyle: {
    alignSelf: 'center',
    padding: 5,
    paddingRight: 5,
    fontSize: 35,
    color: 'rgb(45, 50, 37)',
  },
  TouchableOpacityStyle: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
};
const {
  TouchableOpacityStyle,
  iconStyle,
  mainTextStyle,
  secondTextStyle,
  imageStyle,
  nameContainerStyle,
} = styles;


class DishDescription extends React.Component<*> {
  // Set the navigation options for `react-navigation`
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return {
      headerTitle: <DishDetailHeader
        dish={params.dish}
        onPress={() => { navigation.goBack(null); }}
                   />,
      headerStyle: { height: 300 },
      headerLeft: null,
    };
  };

  render() {
    const { dish } = this.props.navigation.state.params;
    console.log('dishhhhh', dish);
    return (
      <Screen>
        <ScrollView>
          {renderIf(
            dish.vegetarian,
            <View style={TouchableOpacityStyle}>
              <Icon name="md-leaf" style={iconStyle} />
              <Text style={secondTextStyle}>המנה צמחונית</Text>
            </View>,
          )}
          {renderIf(
            dish.vegan,
            <View style={TouchableOpacityStyle}>
              <Image source={require('./../../loggedin/images/veganBlack.png')} style={imageStyle} />
              <Text style={secondTextStyle}>המנה טבעונית</Text>
            </View>,
          )}
          <Text style={mainTextStyle}>תיאור המנה:</Text>
          <Text style={secondTextStyle}>{dish.dishDescription} </Text>
          <Text style={mainTextStyle}>מרכיבים:</Text>
          <Text style={secondTextStyle}>{dish.ingredients}</Text>
          <Text style={mainTextStyle}>אלרגנים:</Text>
          <View style={nameContainerStyle}>
            {renderIf(
              dish.allergies,
              <View style={TouchableOpacityStyle}>
                <Text style={secondTextStyle}>המנה מכילה { dish.allergies }</Text>
              </View>,
            )}

          </View>
        </ScrollView>
      </Screen>
    );
  }
}

export default DishDescription;
