import React from 'react';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import SwipeCards from 'react-native-swipe-cards';
import currentWeekNumber from 'current-week-number';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
import { hideLoading, showLoading } from './../../ui/redux/uiActions';
import { updateUser } from './../../redux/userActions';
import store from './../../redux/store';
import TinderDish from './../../ui/components/TinderDish';
import Button from './../../ui/components/Button';
import { weeklyMenu } from './../../redux/WeeklyDishesActions';
import CardSection from './../../ui/components/card/CardSection';

import Icon from '../../ui/components/Icon';
import Screen from '../../ui/components/Screen';

const styles = {
  drawerIcon: {
    fontSize: 32,
    marginLeft: 10,
  },
  itemIcon: {
    fontSize: 32,
  },
  thumbnailStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    borderRadius: 100,
    width: 150,
  },

  smallThumbnailStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    height: 50,
    borderRadius: 30,
    width: 60,
    backgroundColor: 'rgb(228, 75, 0)',
    flex: 1,
  },

  buttonContainerStyle: {
    paddingTop: 20,
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    flexDirection: 'row',
    borderColor: '#ddd',
    flex: 1,
  },

  notInBaseTextStyle: {
    // alignSelf: 'center',
    // justifyContent: 'center',
    // alignItems: 'center',
    textAlign: 'center',
  },

  iconStyle: {
    fontSize: 65,
    width: 65,
    margin: 10,
    color: 'rgb(255, 255, 255)',
  },

  dateContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderRadius: 100,
    width: '100%',
    backgroundColor: 'rgb(164, 155, 151)',
    borderColor: '#ddd',
    borderBottomWidth: 1,
  },

  dateTextStyle: {
    fontSize: 23,
    color: 'rgb(254, 254, 254)',
    fontWeight: 'bold',
    marginLeft: 8,
    marginRight: 8,
    textAlign: 'center',
    flex: 6,
  },

};

// returns week in ww_yyyy format
function getCurrentWeek(date) {
  const weekNum = currentWeekNumber(date);
  const currWeek = date.getFullYear().toString() + "_" + weekNum.toString();
  return currWeek;
}

class Feed extends React.Component<*> {
  // Set the navigation options for `react-navigation`
  static navigationOptions = ({ navigation }) => ({
    headerLeft: <Icon name="md-menu" style={styles.drawerIcon} onPress={() => navigation.navigate('DrawerOpen')} />,
    headerTitle: 'פיד',
    drawerIcon: <Icon name="md-restaurant" style={styles.itemIcon} />,

  });

  constructor(props) {
    super(props);
    this.props = ({
      dish: [],
    });
    this.state = ({

    });
  }

  render() {
    // take date from the navigator
    const day = new Date();
    // get what day of the week is the date
    const dayInWeek = day.getDay();
    // get the name of the day (ראשון, שני...)
    const dayName = this.getDayName(dayInWeek);
    console.log('this props ', this.props);
    const emptyArray = [];                       // change this.props.menu[3].day to this.props.menu[dayInWeek].day!!!!!!!!!!!!!!!!!!!!
    const menu = (this.props.menu.length > 0) ? this.keyValueToIncremental(this.props.menu[0].day) : emptyArray;
    console.log('the new menu is:', menu);
    return (
      <Screen>
        {/* this section is to show the day that the user is choosing the food for */}
        <CardSection>
          <View style={styles.dateContainerStyle}>
            <Text style={styles.dateTextStyle}> { dayName } </Text>
            <Button
              onPress={() => this.notInBasebuttonPress(this.props)}
              text="לא בבסיס"
              textStyle={styles.notInBaseTextStyle}
              containerStyle={styles.smallThumbnailStyle}
            />
          </View>
        </CardSection>

        <SwipeCards
          ref={'swiper'}
          cards={menu}
          loop
          dragY
          showYup
          showNope
          showMaybe={false}
          handleYup={({ dish, dishID }) => this.handleYup(dish, dishID, this.props)}
          handleNope={this.handleNope}
          onClickHandler={({ dish }) => {console.log('clicked'); this.props.navigation.navigate('dishTabs', dish); }}

          renderCard={({ dish, dishID }) => (
            <View>
              <TouchableOpacity onPress={() => { console.log('clicked', dish); this.props.navigation.navigate('DishTabs', { dish }); }}>
                <TinderDish
                  dishName={dish.dishName}
                  dishPicture={dish.dishPicture}
                  dishDescription={dish.dishDescription}
                  allergies={dish.allergies}
                  vegan={dish.vegan}
                  vegetarian={dish.vegetarian}
                  raters={dish.raters}
                  rating={dish.rating}
                  calories={dish.calories}
                />
              </TouchableOpacity>
              <View style={styles.buttonContainerStyle}>
                <Button
                  onPress={() => this.buttonYupPress(dish, dishID, this.props, this.refs)}
                  // text="Swipe Right"
                  icon="md-thumbs-up"
                  iconStyle={styles.iconStyle}
                  containerStyle={styles.thumbnailStyle}
                />
                <Button
                  onPress={() => this.refs.swiper._forceLeftSwipe()}
                  // text="Swipe Left"
                  icon="md-thumbs-down"
                  iconStyle={styles.iconStyle}
                  containerStyle={styles.thumbnailStyle}
                />
              </View>
            </View>
          )} // renderItem
        />
      </Screen>
    );
  }

  // this function handles the swipe right
  // gets the dish and pass it to the summary screen
  handleYup = (dish, dishID, props) => {
    console.log(props.navigation.state.params.date);
    props.navigation.navigate('Summary', { dishID, dishSelected: dish, date: props.navigation.state.params.date });
  }
  // this function handles the button right
  // gets the dish and pass it to the summary screen
  buttonYupPress = (dish, dishID, props, refs) => {
    refs.swiper._forceRightSwipe();
    props.navigation.navigate('Summary', { dishID, dishSelected: dish, date: props.navigation.state.params.date });
  }

  notInBasebuttonPress = (props) => {
    Alert.alert(
      'וידאו הזמנה',
      'האם את/ה בטוח לא אוכל/ת בבסיס',
      [{ text: 'בטל' },
        { text: 'המשך', onPress: () => this.alertPress(props) }],
      { cancelable: false },
    );
  }

  alertPress = (props) => {
    const { date } = props.navigation.state.params;
    const day = new Date(date);
    const DB = firebase.firestore();
    console.log('the props are', props);
    const { uid, base, catering } = props.firestoreUser;
    const week = getCurrentWeek(day); // put this in .doc('2018_18') && doc('2018_1')
    const userRef = firebase.firestore().collection('Users').doc(uid).collection('Weeks').doc(week);
    const dayRef = firebase.firestore().collection('Caterings').doc(catering)
      .collection('Bases').doc(base).collection('Weeks').doc('2018_18').collection('Days').doc('2018-04-29'); // date
    try {
      DB.runTransaction((transaction) => {
        console.log('started transaction');
        return transaction.get(dayRef).then((doc) => {
          /* this is the catering DB update */
          if (!doc.exists) { // the document of that specific week doesn't exixt in DB yet
            console.log('document does not exist');
            transaction.set(dayRef, { notInBase: { name: 'לא בבסיס', ordered: 1 } });
          } else { // doc exists
            const docData = doc.data();
            let newOrdered = 1;
            if (docData.notInBase) { // notInBase already exists in document
              newOrdered = docData.notInBase.ordered + 1;
            }
            transaction.update(dayRef, { notInBase: { name: 'לא בבסיס', ordered: newOrdered } }); // update ordered cuantity in dish
          }
          /* this is the user DB update */
          transaction.update(userRef, { [`${date}`]: [{
            date,
            notInBase: true }],
          });
          /* this is the redux user update */
          props.firestoreUser.currentWeek[`${date}`] = [{
            date,
            notInBase: true,
          }];
        });
      }).then(() => {
        console.log('i am in the dot then');
        // dishpatch the action of the new user to redux
        store.dispatch(showLoading());
        store.dispatch(updateUser(props.firestoreUser), () => store.dispatch(hideLoading()));
        // navigate to AgendaScreen without option to come back to summary
        const resetNavigationRoute = NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'AgendaScreen' })],
        });
        props.navigation.dispatch(resetNavigationRoute);
      }).catch((error) => {
        console.log('transacrion error', error);
      });
    } catch (error) { console.log('error in Summary', error); }
  }


  // this function hadles the nope
  // nothing to do with this right now
  handleNope = (card) => {
  }

  // this function gets the day in week as a number 0 = sunday...
  // and return the "name" of that day
  getDayName = (day) => {
    switch (day) {
      case 0:
        return 'ראשון';
      case 1:
        return 'שני';
      case 2:
        return 'שלישי';
      case 3:
        return 'רביעי';
      case 4:
        return 'חמישי';
      case 5:
        return 'שישי';
      case 6:
        return 'שבת';
      default:
        // if this option is choosen there was a problem
        return 'איזה יום היום?';
    }
  }

  // this function converts the object menu in props to an emptyArray
  // for the component SwipeCards.
  keyValueToIncremental = (arr) => {
    const newArr = [];
    Object.keys(arr).forEach((dish) => {
      newArr.push({
        dish: arr[dish],
        dishID: dish,
      });
    });
    return newArr;
  }
}

function mapStateToProps(state) {
  return {
    menu: state.menu,
    firestoreUser: state.firestoreUser,
  };
}

export default connect(mapStateToProps, { weeklyMenu })(Feed);
