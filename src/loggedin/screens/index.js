/**
 * @flow
 *
 * This file sets up our Logged In screens.
 *
 * We use `react-navigation` for all navigation as it is the current standard JS navigation library
 * for react-native
 */
import React from 'react';
import { StyleSheet } from 'react-native';
import { StackNavigator, DrawerNavigator, TabNavigator, TabBarBottom } from 'react-navigation';

import Icon from '../../ui/components/Icon';

// Load all our logged in screens
import ChangeEmail from '../../auth/email/screens/ChangeEmail';
import ChangePassword from '../../auth/email/screens/ChangePassword';
import LinkEmail from '../../auth/email/screens/LinkEmail';
import LinkPhone from '../../auth/phone/screens/LinkPhone';
import LoggedInHome from './LoggedInHome';
import Profile from './Profile';
import AgendaScreen from './AgendaScreen';
import ReAuthScreen from '../../auth/core/screens/ReAuthScreen';
import LinkAddress from './LinkAddress';
import EditAllergies from './Allergies';
import CustomDrawerContentComponent from '../../navigation/drawer';
// the feed screen is in development --- ilan
import Feed from './Feed';
import DishComments from './DishComments';
import DishDescription from './DishDescription';
import Rating from './Rating';
import Summary from './Summary';
import * as Theme from '../../theme';


const styles = StyleSheet.create({
  icon: {
    fontSize: 30,
  },
  drawerIcon: {
    fontSize: 32,
    marginLeft: 10,
  },
});


const DishTabs = TabNavigator({
  DishDescription: {
    navigationOptions: {
      tabBarLabel: 'תיאור',
    },
    screen: DishDescription,
  },
  DishComments: {
    navigationOptions: {
      tabBarLabel: 'תגובות',
    },
    screen: DishComments,
  },
}, {
  initialRouteName: 'DishDescription',
  tabBarOptions: {
    activeTintColor: Theme.PRIMARY,
    labelStyle: {
      fontSize: 26,
    },
    style: {
      borderColor: '#ddd',
      borderRadius: 5,
      backgroundColor: '#fff',
      borderBottomWidth: 1,
      padding: 5,
    },
  },
  tabBarComponent: TabBarBottom,
  tabBarPosition: 'top',
});


/*
 * We use a StackNavigator for the Home tab. This allows screens to stack on top of each
 * other and to navigate backwards and forwards between them.
 *
 * Find out more: https://reactnavigation.org/docs/navigators/stack
 */
const HomeStack = StackNavigator({
  LoggedInHome: { screen: LoggedInHome },
}, {
  // Explicitly set the default screen to use
  initialRouteName: 'LoggedInHome',
});


const AgendaStack = StackNavigator({
  AgendaScreen: { screen: AgendaScreen },
  Feed: { screen: Feed },
  Summary: { screen: Summary },
  DishTabs: { screen: DishTabs },
  Rating: { screen: Rating },
}, {
  // Explicitly set the default screen to use
  initialRouteName: 'AgendaScreen',
});

/*
 * We use a StackNavigator for the Profile tab. This allows screens to stack on top of each
 * other and to navigate backwards and forwards between them.
 *
 * Find out more: https://reactnavigation.org/docs/navigators/stack
 */
const ProfileStack = StackNavigator({
  ChangeEmail: { screen: ChangeEmail },
  ChangePassword: { screen: ChangePassword },
  LinkEmail: { screen: LinkEmail },
  LinkPhone: { screen: LinkPhone },
  LinkAddress: { screen: LinkAddress },
  EditAllergies: { screen: EditAllergies },
  Profile: { screen: Profile },

}, {
  // Explicitly set the default screen to use
  initialRouteName: 'Profile',

});

// drawer stack
const DrawerStack = DrawerNavigator({
  Home: {
    screen: HomeStack,
    navigationOptions: {
      drawerLabel: 'בית',
    },
  },
  Agenda: {
    screen: AgendaStack,
    navigationOptions: {
      drawerLabel: 'יומן',
    },
  },
  Profile: {
    screen: ProfileStack,
    navigationOptions: {
      drawerLabel: 'פרופיל',
    },
  },
}, {
  initialRouteName: 'Home',
  drawerWidth: 250,
  drawerPosition: 'right',
  contentComponent: CustomDrawerContentComponent,

});

/*
 * We use a StackNavigator as the basis for the logged in screens. This allows us to present the
 * re-authentication screen over the top of any other screen.
 *
 * Find out more: https://reactnavigation.org/docs/navigators/stack
 */
export default StackNavigator({
  Tabs: {
    screen: DrawerStack,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <Icon name="md-menu" style={styles.drawerIcon} onPress={() => navigation.navigate('DrawerOpen')} />,
    }),
  },
  ReAuth: { screen: ReAuthScreen },
}, {
  cardStyle: {
    backgroundColor: 'transparent',
    opacity: 1,
  },
  headerMode: 'none',
  initialRouteName: 'Tabs',
  mode: 'modal',
});
