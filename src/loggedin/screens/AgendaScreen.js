import React, { Component } from 'react';
import firebase from 'react-native-firebase';
import {  Text,  View,  StyleSheet,  Image, TouchableOpacity } from 'react-native';
import { Agenda } from 'react-native-calendars';
import Moment from 'react-moment';
import moment from 'moment';
import { connect } from 'react-redux';
import ElevatedView from 'react-native-elevated-view';
import { userLogin, userGetDishes } from '../../redux/userActions';
import { getCurrentDate, getCurrentWeek } from '../../helpers/DateGetter'
import { hideLoading, showLoading } from '../../ui/redux/uiActions';
import Icon from '../../ui/components/Icon';

class AgendaScreen extends React.Component<*> {
  // Set the navigation options for `react-navigation`
  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'יומן',
    headerLeft: <Icon name="md-menu" style={styles.drawerIcon} onPress={() => navigation.navigate('DrawerToggle')} />,
    drawerIcon: <Icon name="md-restaurant" style={styles.itemIcon} />,

  });

  constructor(props) {
    super(props);
    this.state = {
      items: {
      },
    };
  }


  /*
  '2018-04-05': [{ text: 'item 1 - any js object' }],
  '2018-04-06': [{ text: 'item 2 - any js object' }],
  '2018-04-07': [], <-- this is an empty date

  cuurently empty (from DB, if DB has empty dates):
  '2018-04-09': [{[ name:'', url:'', date:'2018-09-04' ]}]
*/

  render() {
    console.log('getsunday', moment().startOf('week').toISOString().substring(0, 10));
    console.log('state at Agendascreen render:', this.state);
    const { firestoreUser } = this.props;
    console.log('AgendaScrenn props:', this.props);
    console.log('dishes', firestoreUser.currentWeek);
    console.log(this.props.menu);
    console.log('items in state:', this.state.items);
    console.log('lasttWeek', getCurrentWeek(1));
    if (firestoreUser) {
      if (!firestoreUser.currentWeek) {
        this.props.dispatch(showLoading());
        this.props.dispatch(userGetDishes(firestoreUser, '2018_18'), () => this.props.dispatch(hideLoading())); // change 10.5
        return null;
      }
      if (!firestoreUser.lastWeek) {
        this.props.dispatch(showLoading());
        this.props.dispatch(userGetDishes(firestoreUser, '2018_17', () => this.props.dispatch(hideLoading())));
        return null;
      }
    } else {
      return null;
    }


    return (
      <Agenda
        items={this.state.items}
        loadItemsForMonth={this.loadItems.bind(this)} // used just for current week
        //{(month) => {console.log('trigger items loading') }}
        selected={'2018-04-29'}
        // () => { console.log('selected:', moment().startOf('week')); return (moment().startOf('week')); }
        renderItem={this.renderItem.bind(this)}
        renderEmptyDate={this.renderEmptyDate.bind(this)}
        rowHasChanged={this.rowHasChanged.bind(this)}
        onCalendarToggled={(calendarOpened) => {console.log('calendarOpened')}}
        // onDayPress={this.dayPressed.bind(this)}
        // showWeekNumbers={true}
        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
        minDate={'2018-04-22'}
        // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
        maxDate={'2018-05-03'}
        // Max amount of months allowed to scroll to the past. Default = 50
        pastScrollRange={1}
        // Max amount of months allowed to scroll to the future. Default = 50
        futureScrollRange={1}
        // markingType={'period'}
        // markedDates={{
        //    '2017-05-08': {textColor: '#666'},
        //    '2017-05-09': {textColor: '#666'},
        //    '2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
        //    '2017-05-21': {startingDay: true, color: 'blue'},
        //    '2017-05-22': {endingDay: true, color: 'gray'},
        //    '2017-05-24': {startingDay: true, color: 'gray'},
        //    '2017-05-25': {color: 'gray'},
        //    '2017-05-26': {endingDay: true, color: 'gray'}}}
        // monthFormat={'yyyy'}
        // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
        // renderDay={(day, item) => (<Text>{day ? day.day : item}</Text>)}
      />
    );
  }

  dayPressed(day) {
    const month = day.month +"_"+ day.year
    const week = month
    console.log('month on dayPressed', month);
  }

  loadItems(day) {
    const { firestoreUser } = this.props;
    console.log('date pressed', day);
    const newItems = {};
    console.log('firestoreUser.currentWeek', firestoreUser.currentWeek);
    Object.keys(firestoreUser.currentWeek).forEach(key => {newItems[key] = firestoreUser.currentWeek[key];});
    console.log('newItems[]:', newItems);
    this.setState({
      items: newItems
    });
    Object.keys(firestoreUser.lastWeek).forEach(key => {newItems[key] = firestoreUser.lastWeek[key];});
    console.log('newItems[]:', newItems);
    this.setState({
      items: newItems,
    });
  } // loadItems

  renderItem(item) {
    const diffFromToday = moment('2018-04-29').diff(new Date(item.date)) / 86400000;
    console.log(diffFromToday, 'diffFromToday');
    // if date passed and wasnt ranked
    if (item.hasOwnProperty('notInBase')) {
      if (diffFromToday > 1) {
        return (
          <View style={styles.emptyDatePast}>
            <ElevatedView
              elevation={3}
              style={styles.notInBaseStayElevated}>
              <Text style={{ fontSize: 15, fontWeight: 'bold', marginRight: 10 }}>לא הייתי בבסיס בתאריך זה</Text>
            </ElevatedView>
          </View>
        );
      }
      return (
        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.notInBasePress(item)}>
          <View style={styles.notInBaseItem}>
            <ElevatedView
              elevation={3}
              style={styles.stayElevated}>
              <Text style={{ fontSize: 15, fontWeight: 'bold', marginRight: 10 }}>אני לא בבסיס. לחץ לשינוי</Text>
            </ElevatedView>
          </View>
        </TouchableOpacity>
      );
    } else if ((!item.hasOwnProperty('ranked')) && diffFromToday > 1) {
      return (
        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.ratingPress(item)}>
          <View style={styles.lateItem}>
            <ElevatedView
              elevation={3}
              style={styles.stayElevated}>
              <Image
                style={{ width: 50, height: 50 }}
                source={{ uri: item.pic }}
              />
              <Text style={{ margin: 13, alignSelf: 'center', fontWeight: 'bold', fontSize: 20 }}>{item.name}</Text>
            </ElevatedView>
          </View>
        </TouchableOpacity>

      );
    } else if (item.hasOwnProperty('ranked')) { // if rankedItem
      return (
        <TouchableOpacity style={{ flex: 1 }} onPress={() => console.log("GO TO Summary")}>
          <View style={styles.rankedItem}>
            <ElevatedView
              elevation={3}
              style={styles.stayElevated}>
              <Image
                style={{ width: 50, height: 50 }}
                source={{ uri: item.pic }}
              />
              <Text style={{ margin: 13, alignSelf: 'center', fontWeight: 'bold', fontSize: 20 }}>{item.name}</Text>
            </ElevatedView>
          </View>
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity style={{ flex: 1 }} onPress={() => this.onItemPress(item)}>
        <View style={styles.item}>
          <ElevatedView
            elevation={3}
            style={styles.stayElevated}>
            <Image
              style={{ width: 50, height: 50 }}
              source={{ uri: item.pic }}
            />
            <Text style={{ margin: 13, alignSelf: 'center', fontWeight: 'bold', fontSize: 20 }}>{item.name}</Text>
          </ElevatedView>
        </View>
      </TouchableOpacity>
    );
  } // render Item

  onItemPress = (item) => {
    this.props.navigation.navigate('Summary', { date: item.date, dishID: item.dishID });
    console.log('item chosen', item);
  }
  notInBasePress = (item) => {
    this.props.navigation.navigate('Summary', { date: item.date, notInBase: true });
    console.log('item chosen', item);
  }

  ratingPress = (item) => {
    const dishSelectedWeek = getCurrentWeek(item.date);
    const currWeek = getCurrentWeek(new Date()); // new Date()
    if (dishSelectedWeek !== currWeek) { // the dish is from last week
      const { catering } = this.props.firestoreUser;
      // make a reference to the dishes collection of the catering   <------- change 'EXAMPLE' to 'catering'!!!!!!!!
      const dishesRef = firebase.firestore().collection('Caterings').doc('EXAMPLE')
        .collection('Dishes').doc(item.dishID);
      // get the dish from DB
      dishesRef.get()
        .then((dishSelected) => {
          console.log('the dish selected is:', dishSelected.data());
          // navigate to rating with date and dishSelected
          this.props.navigation.navigate('Rating', { date: item.date, dishSelected: dishSelected.data(), dishID: item.dishID });
        })
        .catch((error) => {
          console.log(`error getting the selected dish from DB = ${error}`);
        });
    } else {
      // the dish is from this week, send to rating with dishID (not all the dish) because it apeares in menu
      this.props.navigation.navigate('Rating', { date: item.date, dishID: item.dishID });
    }
  }

  renderEmptyDate(item) {
    const diffFromToday = moment('2018-04-29').diff(new Date(item.toISOString())) / 86400000;
    if (diffFromToday > 0) {
      return (
        <View style={styles.emptyDatePast}>
          <ElevatedView
            elevation={3}
            style={styles.notInBaseStayElevated}>
            <Text style={{ fontSize: 15, fontWeight: 'bold', marginRight: 10 }}>לא הוזמנה מנה בתאריך זה</Text>
          </ElevatedView>
        </View>
      );
    }
    return (
      <TouchableOpacity style={{ flex: 1 }} onPress={() => this.onEmptyItemPress(item)}>
        <View style={styles.emptyDate}>
          <ElevatedView
            elevation={3}
            style={styles.stayElevated}>
            <Text style={{ fontSize: 15, marginRight: 10 }}>לחץ לבחירת מנה</Text>
          </ElevatedView>
        </View>
      </TouchableOpacity>
    );
  }
  onEmptyItemPress = (item) => {
    const date = item.toISOString().substring(0, 10);
    console.log('onEmptyItemPress', date);
    this.props.navigation.navigate('Feed', { date });
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }
} // class Agendascreen

const styles = StyleSheet.create({
  drawerIcon: {
    fontSize: 32,
    marginLeft: 10,
  },
  itemIcon: {
    fontSize: 32,
  },
  stayElevated: {
    backgroundColor: 'white', // 'rgba(122, 247, 251, 0.15)',
    padding: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderRadius: 5,
    height: 60,
    width: '97%',
    alignSelf: 'center',
    alignItems: 'center',
  },
  notInBaseStayElevated: {
    backgroundColor: 'rgba(196, 196, 196, 0.4)', // 'rgba(122, 247, 251, 0.15)',
    padding: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderRadius: 5,
    height: 60,
    width: '97%',
    alignSelf: 'center',
    alignItems: 'center',
  },
  item: {
    borderRadius: 5,
    marginRight: 10,
    marginTop: 17,
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
  },
  lateItem: {
    borderRadius: 5,
    marginRight: 10,
    marginTop: 17,
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: 'rgba(255, 0, 0, 0.1)',
  },
  rankedItem: {
    marginRight: 10,
    marginTop: 17,
    backgroundColor: 'rgba(13, 255, 0, 0.1)',
    flex: 1,
    // alignItems: 'center',
    borderRadius: 5,
    justifyContent: 'center',
    flexDirection: 'column',
  },

  emptyDate: {
    borderRadius: 5,
    marginRight: 10,
    marginTop: 17,
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',

  },
  emptyDatePast: {
    marginRight: 10,
    marginTop: 17,
    // backgroundColor: 'rgba(33, 33, 33, 0.1)',
    flex: 1,
    alignItems: 'center',
    borderRadius: 5,
    justifyContent: 'center',
    flexDirection: 'column',
  },
  notInBaseItem: {
    borderRadius: 5,
    marginRight: 10,
    marginTop: 17,
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
  },
});

const stateToProps = (state) => {
  return { firestoreUser: state.firestoreUser, menu: state.menu };
};

export default connect(stateToProps)(AgendaScreen);
