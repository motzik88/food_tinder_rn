import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import type { NavigationScreenProp } from 'react-navigation/src/TypeDefinition';
import type { FormProps } from 'redux-form';

import { hideLoading, showLoading } from '../../ui/redux/uiActions';
import FormError from '../../ui/components/form/FormError';
import SubmitButton from '../../ui/components/form/SubmitButton';
import TextField from '../../ui/components/form/TextField';
import { updateAddress } from '../../helpers/user';
import { updateUser } from '../../redux/userActions';
import { showMessage } from '../../ui/components/Toast';


type Props = {
  // Whether to prompt for re-authentication
  allowReAuth: boolean,
  // Whether to autofocus the first field
  autoFocus?: boolean,
  // The text to show on the submit button
  buttonText: string,
  // Whether to show a name field (for registration)
  collectName: boolean,
  navigation: NavigationScreenProp<*, *>,
  // Called when Firebase has sent a verification code
  onVerificationCodeSent: (string) => any,
  // Called if Firebase auto-verifies the phone number (Android only)
  onVerified: (Object, ?string) => any,
  // Whether we are re-authenticating the current user
  reAuth: boolean,
} & FormProps;

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    flex: 1,
  },
});


class LinkAddress extends React.Component<Props> {
  AddressInput: ?Field;

  static defaultProps = {
    autoFocus: false,
  }
  static navigationOptions = {
    headerTitle: 'עריכת כתובת ',
  };
  componentWillMount() {
    if (this.props.firestoreUser.address) {
      const { city, street, number, apartment } = this.props.firestoreUser.address;
      this.props.initialize({ city, street, number, apartment });
    }
  }

  render() {
    const {
      error,
      handleSubmit,
      invalid,
      submitting,
    } = this.props;

    return (
      <ScrollView keyboardShouldPersistTaps="handled" style={styles.container}>
        <FormError error={error} />
        <Field
          component={TextField}
          value="what"
          name="city"
          placeholder="עיר"
          onSubmitEditing={() => this.focusNext(this.streetInput)}
          ref={(ref) => { this.cityInput = ref; }}
          withRef
        />
        <Field
          component={TextField}
          name="street"
          onSubmitEditing={() => this.focusNext(this.numberInput)}
          placeholder="רחוב"
          ref={(ref) => { this.streetInput = ref; }}
          withRef
        />
        <Field
          component={TextField}
          name="number"
          onSubmitEditing={() => this.focusNext(this.apartmentInput)}
          placeholder="מספר"
          ref={(ref) => { this.numberInput = ref; }}
          withRef
        />
        <Field
          component={TextField}
          name="apartment"
          onSubmitEditing={handleSubmit(this.onSubmit)}
          placeholder="דירה"
          ref={(ref) => { this.apartmentInput = ref; }}
          withRef
        />
        <SubmitButton
          disabled={invalid && !error}
          loading={submitting}
          onPress={handleSubmit(this.onSubmit)}
          text="עדכן כתובת"
        />
      </ScrollView>
    );
  }

  focusNext = (nextInput) => {
    // Redux Form exposes a `getRenderedComponent()` method to get the inner TextField
    if (nextInput) nextInput.getRenderedComponent().focus();
  }

  /*
   * This is triggered when the user submits the form
  */
  onSubmit = (values: Object) => {
    const { navigation } = this.props;
    const { uid } = this.props.firestoreUser;
    const newUser = this.props.firestoreUser;
    newUser.address = values;
    // 1) Show the loading screen
    this.props.dispatch(showLoading());
    // return a promise that redux-form can handle and knows that the form is being submitted
    return updateAddress(uid, values)
      .then(() => {
        // updates redux user
        this.props.dispatch(updateUser(newUser));
        // navigaitng back to pofile
        navigation.goBack();
        showMessage('כתובת עודכנה בהצלחה');
        // 1) Hide the loading screen
        this.props.dispatch(hideLoading());
      });
  }
}

const stateToProps = (state) => {
  return {
    firestoreUser: state.firestoreUser,
  };
};

// withNavigation ensures that the component has access to the navigation object
// reduxForm allows `redux-form` to manage the underlying form and its fields
export default connect(stateToProps)(withNavigation(reduxForm({
  form: 'Address',
})(LinkAddress)));
