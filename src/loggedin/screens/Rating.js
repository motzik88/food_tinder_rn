import firebase from 'react-native-firebase';
import React from 'react';
import { NavigationActions } from 'react-navigation';
import StarRating from 'react-native-star-rating';
import { View, Text, ImageBackground, TextInput } from 'react-native';
import { connect } from 'react-redux';
import currentWeekNumber from 'current-week-number';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Button from './../../ui/components/Button';
import { weeklyMenu } from './../../redux/WeeklyDishesActions';
import CardSection from './../../ui/components/card/CardSection';
import { hideLoading, showLoading } from './../../ui/redux/uiActions';
import { updateUser } from './../../redux/userActions';
import store from './../../redux/store';
import Screen from '../../ui/components/Screen';

const styles = {
  buttonStyle: {
    justifyContent: 'center',
    // alignItems: 'center',
    height: 40,
    borderRadius: 10,
    width: '80%',
    marginTop: 10,
    // flex: 1,
    alignSelf: 'center',
  },

  buttonTextStyle: {
    fontWeight: 'bold',
    fontSize: 20,
  },

  bottomContainerStyle: {
    justifyContent: 'space-between',
    flexDirection: 'column',
    borderColor: '#ddd',
    borderBottomWidth: 1,
    padding: 5,
    height: 150,
    backgroundColor: '#fff',
    // position: 'relative',
    alignItems: 'center',
    // flex: 1,
  },

  updateUntilContainerStyle: {
    backgroundColor: 'rgba(255, 156, 52, 0.75)',
    // borderColor: 'rgb(13, 237, 245)',
    // borderStyle: 'dashed',
    borderWidth: 2,
    width: '80%',
    flex: 3,
    justifyContent: 'center',
    borderColor: 'rgba(255, 156, 52, 1)',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },

  updateUntilTextStyle: {
    fontSize: 23,
    color: 'rgb(254, 254, 254)',
    fontWeight: 'bold',
    marginLeft: 8,
    marginRight: 8,
    textAlign: 'center',
    textShadowColor: '#000',
    shadowOpacity: 0.7,
    textShadowOffset: { width: 1, height: 1 },
    // alignSelf: 'center',
  },

  dateContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderRadius: 100,
    width: '100%',
    backgroundColor: 'rgb(164, 155, 151)',
    borderColor: '#ddd',
    borderBottomWidth: 1,
  },

  dateTextStyle: {
    fontSize: 23,
    color: 'rgb(254, 254, 254)',
    fontWeight: 'bold',
    marginLeft: 8,
    marginRight: 8,
    textAlign: 'center',
  },

  dishImageStyle: {
    height: 250,
    width: null,
    flex: 1,
    justifyContent: 'flex-end',
    flexDirection: 'column',
  },

  nameContainerStyle: {
    backgroundColor: 'rgba(45, 50, 37, 0.7)',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  nameTextStyle: {
    fontSize: 40,
    color: 'rgb(254, 254, 254)',
    fontWeight: 'bold',
    marginLeft: 8,
    marginRight: 8,
    flex: 1,
    textAlign: 'left',
  },

  starsContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  labelStarsStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignSelf: 'center',
  },

  textInputContainerStyle: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flex: 2,
  },

  labelStarsTextStyle: {
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 30,
  },

  commentTextStyle: {
    fontSize: 18,
    textAlign: 'left',
    lineHeight: 30,
    marginTop: 8,
  },

  textInputStyle: {
    height: 100,
    borderColor: 'gray',
    borderWidth: 1,
    width: '100%',
    textAlign: 'right',
    flexDirection: 'column',
    // justifyContent: 'flex-start',
    alignItems: 'flex-start',
    // flex: 1,
    paddingHorizontal: 20,
    fontSize: 20,
  },

  containerStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    borderColor: '#ddd',
    position: 'relative',
  },


};

// returns week in ww_yyyy format
function getCurrentWeek(date) {
  const weekNum = currentWeekNumber(date);
  const currWeek = date.getFullYear().toString() + "_" + weekNum.toString();
  return currWeek;
}

class Rating extends React.Component<*> {
  // Set the navigation options for `react-navigation`
  static navigationOptions = {
    headerTitle: 'דירוג מנה',
  };
  constructor(props) {
    super(props);
    this.props = ({
    });
    this.state = ({
      dishRating: 0,
      generalRating: 0,
      text: '',
    });
  }

  render() {
    const { dishSelected, dishID, date } = this.props.navigation.state.params;
    // take date from the navigator
    const day = new Date(date);
    // get what day of the week is the date
    const dayInWeek = day.getDay();
    // get the name of the day (ראשון, שני...)
    const dayName = this.getDayName(dayInWeek);
    // initiate flag for redux update, lastWeek: false then the dish is from the current week
    let lastWeek = false;
    // get the dish
    let dish = [];
    if (dishSelected) {
      // the dish is from last week
      dish = dishSelected;
      lastWeek = true; // set flag for the insertation to redux
    } else {
      // the dish is in menu => eated this week
      dish = this.props.menu[0].day[dishID]; // 0 => dayInWeek
    }

    return (
      <Screen>
        {/* this section is to show the day that the user is choosing the food for */}
        <KeyboardAwareScrollView>
          <CardSection>
            <View style={styles.dateContainerStyle}>
              <Text style={styles.dateTextStyle}> { dayName } </Text>
            </View>
          </CardSection>
          <CardSection>
            {/* This section is for the picture of the dish */}
            <ImageBackground
              source={{ uri: dish.dishPicture }}
              style={styles.dishImageStyle}>
              <View style={styles.nameContainerStyle}>
                {/* This section is for the name of the dish */}
                <Text style={styles.nameTextStyle}>{dish.dishName}</Text>
              </View>
            </ImageBackground>
          </CardSection>
          <View style={styles.containerStyle}>
            <View style={styles.starsContainerStyle}>
              <View style={styles.labelStarsStyle}>
                {/* This section is for the name of the dish */}
                <Text style={styles.labelStarsTextStyle}>דירוג המנה</Text>
                <StarRating
                  starSize={30}
                  rating={this.state.dishRating}
                  selectedStar={stars => this.onDishRatingPress(stars)}
                  halfStar={require('./../../loggedin/images/star-half.png')}
                  fullStarColor="black"
                  halfStarColor="rgb(255, 245, 0)"
                  // emptyStarColor="rgb(255, 245, 0)"
                />
              </View>
              <View style={styles.labelStarsStyle}>
                <Text style={styles.labelStarsTextStyle}>דירוג החוויה הכללית</Text>
                <StarRating
                  starSize={30}
                  rating={this.state.generalRating}
                  selectedStar={stars => this.onGeneralRatingPress(stars)}
                  halfStar={require('./../../loggedin/images/star-half.png')}
                  fullStarColor="black"
                  halfStarColor="rgb(255, 245, 0)"
                />
              </View>
            </View>
            <Text style={styles.commentTextStyle}>הוספת תגובה</Text>
            <TextInput
              style={styles.textInputStyle}
              onChangeText={text => this.setState({ text })}
              value={this.state.text}
              keyboardType="default"
              maxLength={300}
              multiline
              numberOfLines={10}
            />
            <Button
              disabled={this.state.dishRating === 0 || this.state.generalRating === 0}
              onPress={() => this.onButtonPress(day, dish, dishID, lastWeek)}
              text="אישור"
              textStyle={styles.buttonTextStyle}
              containerStyle={styles.buttonStyle}
            />
          </View>
        </KeyboardAwareScrollView>


        {/*
        <CardSection style={styles.bottomContainerStyle}>
          <View style={styles.updateUntilContainerStyle}>
            <Text style={styles.updateUntilTextStyle}>{  }</Text>
          </View>
          <Button
            disabled={disabled}
            onPress={() => this.onButtonPress(notInBase, update, day, dish)}
            text={buttonText}
            textStyle={styles.buttonTextStyle}
            containerStyle={styles.buttonStyle}
          />
        </CardSection> */}
      </Screen>
    );
  } // render

  // this function handlle the dish rating press and insertit in state
  onDishRatingPress = (stars) => {
    this.setState({
      dishRating: stars,
    });
  }

  // this function handlle the general rating press and insertit in state
  onGeneralRatingPress = (stars) => {
    this.setState({
      generalRating: stars,
    });
  }

  /* button press funtion
    in this function we update the rating and raters in the Dishes colection
    also we update the dish in the week of the user in DB and in redux to RANKED = TRUE
  */
  onButtonPress = (day, dish, dishID, lastWeek) => {
    const { date } = this.props.navigation.state.params;
    const DB = firebase.firestore();
    const { uid, catering, displayName, photoURL } = this.props.firestoreUser;
    const week = getCurrentWeek(day);
    const userRef = firebase.firestore().collection('Users').doc(uid).collection('Weeks')
      .doc(week);
    const dishRef = firebase.firestore().collection('Caterings').doc(catering)
      .collection('Dishes')
      .doc(dishID);
    const reviewRef = dishRef.collection('Reviews').doc();
    console.log('before transaction');
    try {
      DB.runTransaction((transaction) => {
        console.log('started transaction');
        return transaction.get(dishRef).then((doc) => {
          /* this is the dish update in the Dishes DB of the catering */
          const docData = doc.data();
          const oldRating = docData.rating;
          const oldRaters = docData.raters;
          // calculate the real rating that the user choosed 30% general expiriance and 70% the spescific dish
          const userRating = (this.state.generalRating * 0.3) + (this.state.dishRating * 0.7);

          // new number of raters, the old number + 1 (the current user rating)
          const newRaters = oldRaters + 1;
          // the new rating calculated
          const newRating = ((oldRaters * oldRating) + userRating) / newRaters;

          // update the rating in the dish on DB
          transaction.update(dishRef, { rating: newRating, raters: newRaters });

          // update the comment in the Reviews collection of the dishID
          transaction.set(reviewRef, {
            Date: new Date(),
            Rating: userRating,
            Review: this.state.text,
            UID: uid,
            UserName: displayName,
            UserPhoto: photoURL,
          });

          /* this is the user DB update */
          transaction.update(userRef, { [`${date}`]: [{
            date,
            name: dish.dishName,
            pic: dish.dishPicture,
            dishID,
            ranked: true }],
          });
          /* this is the redux user update */
          if (lastWeek) {
            this.props.firestoreUser.lastWeek[`${date}`] = [{
              date,
              name: dish.dishName,
              pic: dish.dishPicture,
              dishID,
              ranked: true,
            }];
          } else {
            this.props.firestoreUser.currentWeek[`${date}`] = [{
              date,
              name: dish.dishName,
              pic: dish.dishPicture,
              dishID,
              ranked: true,
            }];
          }
        });
      }).then(() => {
        console.log('i am in the dot then');
        // dishpatch the action of the new user to redux
        store.dispatch(showLoading());
        store.dispatch(updateUser(this.props.firestoreUser), () => store.dispatch(hideLoading()));
        // navigate to AgendaScreen without option to come back to Rating
        const resetNavigationRoute = NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'AgendaScreen' })],
        });
        this.props.navigation.dispatch(resetNavigationRoute);
      }).catch((error) => {
        console.log('transacrion error', error);
      });
    } catch (error) { console.log('error in Rating', error); }
  }


  // this function gets the day in week as a number 0 = sunday...
  // and return the "name" of that day
  getDayName = (day) => {
    switch (day) {
      case 0:
        return 'ראשון';
      case 1:
        return 'שני';
      case 2:
        return 'שלישי';
      case 3:
        return 'רביעי';
      case 4:
        return 'חמישי';
      case 5:
        return 'שישי';
      case 6:
        return 'שבת';
      default:
        // if this option is choosen there was a problem
        return 'איזה יום היום?';
    }
  }
} // class

function mapStateToProps(state) {
  return {
    menu: state.menu,
    firestoreUser: state.firestoreUser,
  };
}

export default connect(mapStateToProps, { weeklyMenu })(Rating);
