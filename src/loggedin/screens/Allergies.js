import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Field, reduxForm, FieldArray, SubmissionError } from 'redux-form';
import { connect } from 'react-redux';

import type { NavigationScreenProp } from 'react-navigation/src/TypeDefinition';
import type { FormProps } from 'redux-form';

import { hideLoading, showLoading } from '../../ui/redux/uiActions';
import FormError from '../../ui/components/form/FormError';
import CheckBoxField from '../../ui/components/form/CheckBox';
import SubmitButton from '../../ui/components/form/SubmitButton';
import { getRestrictions } from '../../redux/RestrictionsActions';
import { updateUserField } from '../../helpers/user';
import { showMessage } from '../../ui/components/Toast';


type Props = {
  // Whether to prompt for re-authentication
  allowReAuth: boolean,
  // Whether to autofocus the first field
  autoFocus?: boolean,
  // The text to show on the submit button
  buttonText: string,
  // Whether to show a name field (for registration)
  collectName: boolean,
  navigation: NavigationScreenProp<*, *>,
  // Called when Firebase has sent a verification code
  onVerificationCodeSent: (string) => any,
  // Called if Firebase auto-verifies the phone number (Android only)
  onVerified: (Object, ?string) => any,
  // Whether we are re-authenticating the current user
  reAuth: boolean,
} & FormProps;

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    flex: 1,
  },
  text: {
    color: '#3496E7',
    fontSize: 20,
    marginLeft: 15,
  },
});

const renderAllergies = ({ fields }) => {
  // Creating an array of fields compnenets of the allergies
  return fields.map(([key, value], index) => {
    return (
      <Field
        key={index}
        component={CheckBoxField}
        name={key}
        label={value}
      />);
  });
};

class EditAllergies extends React.Component<Props> {
  static navigationOptions = {
    headerTitle: 'עריכת רגישיות',
  };

  componentWillMount() {
    const { restrictions } = this.props;
    // Shows the loading screen if restriction is not yet defined, and hides it when it does
    if (!restrictions) {
      this.props.dispatch(showLoading());
      this.props.dispatch(getRestrictions(() => this.props.dispatch(hideLoading())));
    }
    // This initialize the fileds of the form to the current choises that are in firestoreUser.restrictions
    this.props.initialize(this.props.firestoreUser.restrictions);
  }

  render() {
    const { error, handleSubmit, invalid, submitting, restrictions } = this.props;

    if (!restrictions) return null;

    // Seperating the restriction into 2 objects
    let allergies, otherRestrictions = null;
    restrictions.forEach((doc) => {
      if (doc.id === 'Allergies') {
        allergies = doc.data();
      }
      if (doc.id === 'OtherRestrictions') {
        otherRestrictions = doc.data();
      }
    });

    return (
      <ScrollView keyboardShouldPersistTaps="handled" style={styles.container}>
        <FormError error={error} />
        <View><Text key="text1" style={styles.text}>רגישיות</Text></View>
        <FieldArray name="allergies" component={renderAllergies} fields={Object.entries(allergies)} />
        <View><Text key="text1" style={styles.text}>אחר</Text></View>
        <FieldArray name="otherRestrictions" component={renderAllergies} fields={Object.entries(otherRestrictions)} />
        { /* this.renderAllResterctions() */ }
        <SubmitButton
          disabled={invalid && !error}
          loading={submitting}
          onPress={handleSubmit(this.onSubmit)}
          text="עדכן רגישיות והעדפות"
        />
      </ScrollView>
    );
  }


  /*
   * This is triggered when the user submits the form
  */
  onSubmit = (values: Object) => {
    const { navigation, firestoreUser } = this.props;
    const newUser = firestoreUser;
    newUser.restrictions = values;
    try {
      // 1) Show the loading screen
      this.props.dispatch(showLoading());
      // return a promise that redux-form can handle and knows that the form is being submitted
      return updateUserField(firestoreUser, values, 'restrictions')
        .then(() => {
          // updates redux user
          // this.props.dispatch(userLogin(newUser));
          // navigaitng back to pofile
          navigation.goBack();
          showMessage('רגישיות עודכנו בהצלחה');
          // 1) Hide the loading screen
          this.props.dispatch(hideLoading());
        });
    } catch (error) {
      // If there is an error, pass it back to `redux-form`
      throw new SubmissionError({ _error: error });
    }
  }
}

const stateToProps = (state) => {
  return {
    restrictions: state.restrictions,
    firestoreUser: state.firestoreUser,
  };
};

// withNavigation ensures that the component has access to the navigation object
// reduxForm allows `redux-form` to manage the underlying form and its fields
export default connect(stateToProps)(withNavigation(reduxForm({
  form: 'Restrictions',
})(EditAllergies)));
