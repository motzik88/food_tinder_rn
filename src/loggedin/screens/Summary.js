import firebase from 'react-native-firebase';
import React from 'react';
import { NavigationActions } from 'react-navigation';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import currentWeekNumber from 'current-week-number';
import TinderDish from './../../ui/components/TinderDish';
import Button from './../../ui/components/Button';
import { weeklyMenu } from './../../redux/WeeklyDishesActions';
import CardSection from './../../ui/components/card/CardSection';
import { hideLoading, showLoading } from './../../ui/redux/uiActions';
import { updateUser } from './../../redux/userActions';
import store from './../../redux/store';
import renderIf from './../../loggedin/functions/renderIf';
import Screen from '../../ui/components/Screen';

const styles = {
  buttonStyle: {
    // justifyContent: 'center',
    // alignItems: 'center',
    height: 50,
    borderRadius: 10,
    width: '80%',
    flex: 1,
    alignSelf: 'center',
  },

  buttonTextStyle: {
    fontWeight: 'bold',
    fontSize: 15,
  },

  bottomContainerStyle: {
    justifyContent: 'space-between',
    flexDirection: 'column',
    borderColor: '#ddd',
    borderBottomWidth: 1,
    padding: 5,
    height: 200,
    backgroundColor: '#fff',
    // position: 'relative',
    alignItems: 'center',
    // flex: 1,
  },

  updateUntilContainerStyle: {
    backgroundColor: 'rgba(255, 156, 52, 0.75)',
    // borderColor: 'rgb(13, 237, 245)',
    // borderStyle: 'dashed',
    borderWidth: 2,
    width: '80%',
    flex: 3,
    justifyContent: 'center',
    borderColor: 'rgba(255, 156, 52, 1)',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    marginTop: 10,
    marginBottom: 15,
  },

  updateUntilTextStyle: {
    fontSize: 23,
    color: 'rgb(254, 254, 254)',
    fontWeight: 'bold',
    marginLeft: 8,
    marginRight: 8,
    textAlign: 'center',
    textShadowColor: '#000',
    shadowOpacity: 0.7,
    textShadowOffset: { width: 1, height: 1 },
    // alignSelf: 'center',
  },

  dateContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderRadius: 100,
    width: '100%',
    backgroundColor: 'rgb(164, 155, 151)',
    borderColor: '#ddd',
    borderBottomWidth: 1,
  },

  dateTextStyle: {
    fontSize: 23,
    color: 'rgb(254, 254, 254)',
    fontWeight: 'bold',
    marginLeft: 8,
    marginRight: 8,
    textAlign: 'center',
  },

};

// returns week in ww_yyyy format
function getCurrentWeek(date) {
  const weekNum = currentWeekNumber(date);
  const currWeek = date.getFullYear().toString() + "_" + weekNum.toString();
  return currWeek;
}

// gets date and return the day as string
function dateToString(date) {
  const yyyy = date.getFullYear();
  let mm = date.getMonth();
  let dd = date.getDate();
  if (dd < 10) {
    dd = `0${dd}`;
  }
  if (mm < 10) {
    mm = `0${mm}`;
  }
  return `${dd}/${mm}/${yyyy} 20:00`;
}

class Feed extends React.Component<*> {
  // Set the navigation options for `react-navigation`
  static navigationOptions = {
    headerTitle: 'סיכום הזמנה',
  };
  constructor(props) {
    super(props);
    this.props = ({

    });
    this.state = ({

    });
  }

  render() {
    // test thing here
    console.log('this firestore user is', this.props.firestoreUser);
    // take date from the navigator
    const day = new Date(this.props.navigation.state.params.date);
    // get what day of the week is the date
    const dayInWeek = day.getDay();
    // get the name of the day (ראשון, שני...)
    const dayName = this.getDayName(dayInWeek);
    // get the text to put in the until change text box
    const tempObject = this.changeUntilFunc(new Date(2019, 5, 25)); // day
    const { disabled, changeUntilText } = tempObject;
    // update = false => the dish is from tinder so is new selection
    // update = true => the dish is from agenda so the user is reviewing its choose
    let update = false;
    // get the dish
    let dish = [];
    // buttonText is the text inside the button
    let buttonText = 'אשר הזמנה';
    const { notInBase } = this.props.navigation.state.params;
    if (notInBase) {
      console.log('the user want to change notInBase choise');
      buttonText = 'החלף הזמנה'; // <= text inside the button to show if the user wants to update order
    } else if (this.props.navigation.state.params.dishSelected) {
      // if the dish is in navigator => get from tinder
      dish = this.props.navigation.state.params.dishSelected;
    } else {
      // the dish is in menu => get from agenda
      dish = this.props.menu[0].day[this.props.navigation.state.params.dishID]; // dayInWeek
      update = true; // <= the user is reviewing his choose
      buttonText = 'החלף הזמנה'; // <= text inside the button to show if the user wants to update order
    }
    return (
      <Screen>
        <ScrollView>
          {/* this section is to show the day that the user is choosing the food for */}
          <CardSection>
            <View style={styles.dateContainerStyle}>
              <Text style={styles.dateTextStyle}> { dayName } </Text>
            </View>
          </CardSection>
          <View>
            {renderIf(
              // if the user its in the base then render the regular dish
              !notInBase,
              <TinderDish
                dishName={dish.dishName}
                dishPicture={dish.dishPicture}
                dishDescription={dish.dishDescription}
                allergies={dish.allergies}
                vegan={dish.vegan}
                vegetarian={dish.vegetarian}
                raters={dish.raters}
                rating={dish.rating}
                calories={dish.calories}
              />,
            )}
            {renderIf(
              // if user dont eat in base then render the not in base plate
              notInBase,
              <TinderDish
                dishName="לא אוכל בבסיס"
                dishPicture="https://firebasestorage.googleapis.com/v0/b/foodis-db.appspot.com/o/empty%20plate.jpg?alt=media&token=1f78106e-9dbc-4331-8cef-f47c3f4d6f5b"
                dishDescription=""
                allergies={false}
                vegan={false}
                vegetarian={false}
                raters={0}
                rating={0}
                calories=""
              />,
            )}
            <CardSection style={styles.bottomContainerStyle}>
              <View style={styles.updateUntilContainerStyle}>
                <Text style={styles.updateUntilTextStyle}>{ changeUntilText }</Text>
              </View>
              <Button
                disabled={disabled}
                onPress={() => this.onButtonPress(notInBase, update, day, dish)}
                text={buttonText}
                textStyle={styles.buttonTextStyle}
                containerStyle={styles.buttonStyle}
              />
            </CardSection>
          </View>
        </ScrollView>
      </Screen>
    );
  } // render

  /* button press funtion
     if notInBase = true => the user choosed notInBase and now he wants to eat in base
        => errase his choise in week (user) in DB, decrase the counters of the notInBase by one
     if update = true then the user wants to change his choise
        => errase his choise in week (user) in DB, down the counters of the dish by one
     if update = false then the user is choosing a new dish
        => insert the choise in week (user) in DB, up the counters of the dish by one
    at the end, dishpatch the update to redux
  */
  onButtonPress = (notInBase, update, day, dish) => {
    const { dishID, date } = this.props.navigation.state.params;
    const DB = firebase.firestore();
    const { uid, base, catering } = this.props.firestoreUser;
    const week = getCurrentWeek(day); // put this in .doc('2018_18') && doc('2018_1')
    const userRef = firebase.firestore().collection('Users').doc(uid).collection('Weeks').doc(week);
    const dayRef = firebase.firestore().collection('Caterings').doc(catering)
      .collection('Bases').doc(base).collection('Weeks').doc('2018_18').collection('Days').doc(date);

    /*
      this if is when the user choosed not in base and now wants to update his choise
      inside the if statement the number of notInBase ordered decrase by one and update in the week of base in DB
      also the update the week of user un DB and in redux  => return the day to no choise at all and send to agenda
    */
    if (notInBase) {
      try {
        DB.runTransaction((transaction) => {
          console.log('started transaction');
          return transaction.get(dayRef).then((doc) => {
            /* this is the catering DB update */
            if (!doc.exists) { // the document of that specific week doesn't exixt in DB yet
              console.log('document does not exist - something whent wrong, doc should exist');
            } else { // doc exists
              const docData = doc.data();
              let newOrdered;
              if (!docData.notInBase) { // dish dose not exists in document
                console.log('notInBase does not exist in document - something whent wrong, notInBase should exist in doc because we are updating a choise');
              } else { // update ordered number of dish
                newOrdered = docData.notInBase.ordered - 1;
              }
              transaction.update(dayRef, { notInBase: { name: 'לא בבסיס', ordered: newOrdered } }); // update ordered quantity in dish
            }
            /* this is the user DB update */
            transaction.update(userRef, { [`${date}`]: [] });
            /* this is the redux user update */
            this.props.firestoreUser.currentWeek[`${date}`] = [];
          });
        }).then(() => {
          console.log('i am in the dot then');
          // dishpatch the action of the new user to redux
          store.dispatch(showLoading());
          store.dispatch(updateUser(this.props.firestoreUser), () => store.dispatch(hideLoading()));
          // navigate to feed without option to come back to summary, back button takes to agenda
          const resetNavigationRoute = NavigationActions.reset({
            index: 1,
            actions: [
              NavigationActions.navigate({ routeName: 'AgendaScreen' }),
              NavigationActions.navigate({ routeName: 'Feed', params: { date } }),
            ],
          });
          this.props.navigation.dispatch(resetNavigationRoute);
        }).catch((error) => {
          console.log('transacrion error', error);
        });
      } catch (error) { console.log('error in Summary', error); }
    } else if (update) {
      /*
       this if statement is to update a choise made from the user
       the if erases the choise from redux and from DB of user
       also decreses the dish quantity form the base week DB
       at the end => sends the user back to tinder to make a new choise.
      */
      // send back to tinder and errase old choose from DB and update counters dish
      console.log('i entered to the if statement');
      // send to agenda and update couners
      console.log('before transaction');
      try {
        DB.runTransaction((transaction) => {
          console.log('started transaction');
          return transaction.get(dayRef).then((doc) => {
            /* this is the catering DB update */
            if (!doc.exists) { // the document of that specific day doesn't exixt in DB
              console.log('document does not exist - something whent wrong, doc should exist');
            } else { // doc exists
              const docData = doc.data();
              let newOrdered;
              if (!docData[dishID]) { // dish dose not exists in document
                console.log('dish does not exist - something whent wrong, dish should exist in doc');
              } else { // update ordered number of dish
                newOrdered = docData[dishID].ordered - 1;
              }
              transaction.update(dayRef, { [`${dishID}`]: { name: dish.dishName, ordered: newOrdered } }); // update ordered cuantity in dish
            }
            /* this is the user DB update */
            transaction.update(userRef, { [`${date}`]: [] });
            /* this is the redux user update */
            this.props.firestoreUser.currentWeek[`${date}`] = [];
          });
        }).then(() => {
          console.log('i am in the dot then');
          // dishpatch the action of the new user to redux
          store.dispatch(showLoading());
          store.dispatch(updateUser(this.props.firestoreUser), () => store.dispatch(hideLoading()));
          // navigate to Feed without option to come back to summary, back button takes to agenda
          const resetNavigationRoute = NavigationActions.reset({
            index: 1,
            actions: [
              NavigationActions.navigate({ routeName: 'AgendaScreen' }),
              NavigationActions.navigate({ routeName: 'Feed', params: { date } }),
            ],
          });
          this.props.navigation.dispatch(resetNavigationRoute);
        }).catch((error) => {
          console.log('transacrion error', error);
        });
      } catch (error) { console.log('error in Summary', error); }
    } else {
      /*
       new choise to insert into DB of user and catering
       the if iserts the choise to redux and from DB of user
       also increses the dish quantity form the base week DB
       at the end => sends the user back to agenda, cannot go back to summary
      */
      console.log('i entered to the else statement');
      // send to agenda and update couners
      console.log('before transaction');
      try {
        DB.runTransaction((transaction) => {
          console.log('started transaction');
          return transaction.get(dayRef).then((doc) => {
            /* this is the catering DB update */
            if (!doc.exists) { // the document of that specific week doesn't exixt in DB yet
              console.log('document does not exist');
              transaction.set(dayRef, { [dishID]: { name: dish.dishName, ordered: 1 } });
            } else { // doc exists
              const docData = doc.data();
              let newOrdered = 1;
              if (docData[dishID]) { // dish exists in document
                newOrdered = docData[dishID].ordered + 1;
              }
              console.log('trying to update the db');
              transaction.update(dayRef, { [`${dishID}`]: { name: dish.dishName, ordered: newOrdered } }); // update ordered cuantity in dish
            }
            /* this is the user DB update */
            console.log('user DB update');
            transaction.update(userRef, { [`${date}`]: [{
              date,
              name: dish.dishName,
              pic: dish.dishPicture,
              dishID }],
            });
            /* this is the redux user update */
            this.props.firestoreUser.currentWeek[`${date}`] = [{
              date,
              name: dish.dishName,
              pic: dish.dishPicture,
              dishID,
            }];
          });
        }).then(() => {
          console.log('i am in the dot then');
          // dishpatch the action of the new user to redux
          store.dispatch(showLoading());
          store.dispatch(updateUser(this.props.firestoreUser), () => store.dispatch(hideLoading()));
          // navigate to AgendaScreen without option to come back to summary
          const resetNavigationRoute = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'AgendaScreen' })],
          });
          this.props.navigation.dispatch(resetNavigationRoute);
        }).catch((error) => {
          console.log('transacrion error', error);
        });
      } catch (error) { console.log('error in Summary', error); }
    }
  }


  // this function gets the day in week as a number 0 = sunday...
  // and return the "name" of that day
  getDayName = (day) => {
    switch (day) {
      case 0:
        return 'ראשון';
      case 1:
        return 'שני';
      case 2:
        return 'שלישי';
      case 3:
        return 'רביעי';
      case 4:
        return 'חמישי';
      case 5:
        return 'שישי';
      case 6:
        return 'שבת';
      default:
        // if this option is choosen there was a problem
        return 'איזה יום היום?';
    }
  }

  // this function gets the date that the user wants to put the dish in
  // checks if there is time to change choise (until 1 day before and before 20:00)
  // the function returnes the text to put in the screen
  changeUntilFunc = (dishDate) => {
    let changeUntilText:? string;
    let disabled = false;
    dishDate.setHours(20);
    const today = new Date();
    const timeLeft = dishDate - today;

    // the user can change the choise until a day before at 20:00.
    dishDate.setDate(dishDate.getDate() - 1);

    // 86400000 miliseconds = 24 hours
    if (timeLeft >= 86400000) { // there still time to chage
      const dayText = dateToString(dishDate);
      changeUntilText = `ניתן לשנות את הבחירה עד ${dayText}`;
    } else { // no time to change
      changeUntilText = 'לא ניתן להזמין או לשנות את הבחירה';
      disabled = true;
    }
    return { changeUntilText, disabled };
  }
}

function mapStateToProps(state) {
  return {
    menu: state.menu,
    firestoreUser: state.firestoreUser,
  };
}

export default connect(mapStateToProps, { weeklyMenu })(Feed);
