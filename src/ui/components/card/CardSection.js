import React, { type Node } from 'react';
import { View, StyleSheet } from 'react-native';

/*
 * We use flow type to validate the Props of the component
 */
type Props = {
  // The children to display in the List component
  children: Node,
  style?: StyleSheet.Styles,
}

const styles = StyleSheet.create({
  containerStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative',
  },
});

const CardSection = (props: Props) => {
  return (
    <View style={[styles.containerStyle, props.style]}>
      {props.children}
    </View>
  );
};

export default CardSection;
