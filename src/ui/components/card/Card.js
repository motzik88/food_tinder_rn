import React, { type Node } from 'react';
import { View } from 'react-native';

/*
 * We use flow type to validate the Props of the component
 */
type Props = {
  // The children to display in the List component
  children: Node,
}

const styles = {
  containerStyle: {
    /* BORDERS */
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    /* SHADOW */
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    /* MARGINS */
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
  },
};

const Card = (props: Props) => {
  return (
    <View style={styles.containerStyle}>
      {props.children}
    </View>
  );
};


export default Card;
