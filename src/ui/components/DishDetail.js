import React from 'react';
import { Text, View, Image, ImageBackground } from 'react-native';
import Card from './card/Card';
import CardSection from './card/CardSection';
import Button from './Button';
import Icon from './Icon';
import renderIf from './../../loggedin/functions/renderIf';

/*
 * We use flow type to validate the Props of the component
 */
type Props = {
  // The dish name
  dishName?: string,
  // The dish description
  dishDescription?: string,
  // The cooker name
  cookerName?: string,
  // The dish price
  dishPrice?: number,
  // Whether the delivery is pobible
  delivery?: boolean,
  // if the dish is vegan
  vegan?: boolean,
  // if the dish is vegetarian
  vegetarian?: boolean,
  // if the dish is vegetarian
  vegetarian?: boolean,
  // The action to call when the button is pressed
  onPress: () => any,
  // The URL of the dish main image
  dishImage?: string,
  // The URL of the cooker Image
  cookerImage?: string,
}


const DishDetail = (props: Props) => {
  const styles = {
    headerContentStyle: {
      flexDirection: 'column',
      justifyContent: 'space-around',
      flex: 2,
    },

    optionsContentStyle: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      flex: 2,
    },

    infoContentStyle: {
      flexDirection: 'column',
      justifyContent: 'flex-start',
      width: '100%',
    },
    innerInfoContentStyle: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },

    descriptionContentStyle: {
      justifyContent: 'flex-start',
      flexDirection: 'row',
      padding: 10,
    },

    cookerTextStyle: {
      fontSize: 18,
      textAlign: 'right',
      alignSelf: 'flex-start',
    },

    thumbnailStyle: {
      height: 60,
      borderRadius: 50,
      width: 60,
    },

    thumbnailContainerStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 5,
      marginRight: 5,
      flex: 1,
    },

    dishImageStyle: {
      height: 200,
      width: null,
      flex: 1,
      justifyContent: 'flex-end',
      flexDirection: 'column',
    },

    nameContainerStyle: {
      backgroundColor: 'rgba(45, 50, 37, 0.7)',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },

    nameTextStyle: {
      fontSize: 30,
      color: 'rgb(254, 254, 254)',
      fontWeight: 'bold',
      marginLeft: 8,
      marginRight: 8,
    },

    buttonStyle: {
      flex: 1,
      alignSelf: 'stretch',
      backgroundColor: '#fff',
      borderRadius: 5,
      borderWidth: 1,
      borderColor: '#007aff',
      marginLeft: 5,
      marginRight: 5,
    },
    buttonTextStyle: {
      alignSelf: 'center',
      color: '#007aff',
      fontSize: 16,
      fontWeight: '600',
      paddingTop: 10,
      paddingBottom: 10,
    },
    iconStyle: {
      alignSelf: 'stretch',
      padding: 5,
      fontSize: 30,

    },
  };
  const {
    dishName,
    dishImage,
    cookerImage,
    cookerName,
    dishDescription,
    dishPrice,
    delivery,
    vegan,
    vegetarian,
    onPress,
  } = props;

  const {
    thumbnailStyle,
    cookerTextStyle,
    headerContentStyle,
    nameContainerStyle,
    thumbnailContainerStyle,
    nameTextStyle,
    descriptionContentStyle,
    infoContentStyle,
    optionsContentStyle,
    dishImageStyle,
    innerInfoContentStyle,
    buttonStyle,
    buttonTextStyle,
    iconStyle,
  } = styles;

  return (
    <Card>
      <CardSection>
        {/* This section is for the picture of the dish */}
        <ImageBackground
          source={{ uri: dishImage }}
          style={dishImageStyle}>

          <View style={nameContainerStyle}>
            {/* This section is for the name and price of the dish */}
            <Text style={nameTextStyle}>{dishName}</Text>
            <Text style={nameTextStyle}>{dishPrice}</Text>
          </View>
        </ImageBackground>
      </CardSection>

      <CardSection>
        {/* This section is for the details of the dish */}
        <View style={infoContentStyle}>
          <View style={innerInfoContentStyle}>
            <View style={thumbnailContainerStyle}>
              {/* This section contains the picture of the cooker */}
              <Image
                style={thumbnailStyle}
                source={{ uri: cookerImage }}
              />
            </View>
            <View style={headerContentStyle}>
              {/* This section contains the name of the kitchen and the distance from the eater */}
              <Text style={cookerTextStyle}>{cookerName}</Text>
              <Text style={cookerTextStyle}>2KM</Text>
            </View>
            <View style={optionsContentStyle}>
              {/* This section contains the options of the dish
                we can put here the icons of vegan, delivery and more...
                we have to put here also the rating of the dish
                */}
              {renderIf(
                delivery,
                <Icon name="md-planet" style={iconStyle} />,
              )}
              {renderIf(
                vegan,
                <Icon name="md-leaf" style={iconStyle} />,
              )}
              {renderIf(
                vegetarian,
                <Icon name="md-flower" style={iconStyle} />,
              )}
            </View>
          </View>
          <View style={descriptionContentStyle}>
            {/* This section contains the description of the dish */}
            <Text>{dishDescription}</Text>
          </View>
        </View>
      </CardSection>

      <CardSection>
        {/* This section contains the order now button */}
        <Button
          onPress={() => onPress}
          containerStyle={buttonStyle}
          textStyle={buttonTextStyle}
          text="הזמן"
        />
      </CardSection>

    </Card>
  );
};

export default DishDetail;
