
import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import type { FieldProps } from 'redux-form';
import CheckBox from 'react-native-checkbox';

import * as Theme from '../../../theme';

type Props = {
  // The icon to display as part of the text field
  icon: string,
// The props inherit from the built in `redux-form` FieldProps
} & FieldProps;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#f7f7f7',
    borderColor: Theme.BORDER_COLOR,
    borderRadius: 30,
    borderWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    margin: 6,
  },
  containerError: {
    borderColor: Theme.ERROR_COLOR,
  },
  input: {
    color: '#000',
    flex: 1,
    fontSize: 15,
    height: 25,
    marginLeft: 8,
    marginRight: 16,
    marginTop: 10,
  },
  checkbox: {
    marginTop: 5,
    marginLeft: 20,
    marginRight: 10,
  },
});

export default class CheckBoxField extends React.Component<Props> {
  input: TextInput | null;
  state = {
    checked: this.props.meta.initial || false,
  }


  render() {
    const {
      input: {
        onChange,
        value,
        ...restInput
      },
      meta: {
        error,
        touched,
      },
      ...props
    } = this.props;

    return (
      <View
        style={[styles.container, touched && !!error && styles.containerError]}>
        <CheckBox
          onChange={(val) => {
            this.handleOnChange(!val);
            onChange(!val);
            }
          }
          checked={this.state.checked}
          labelStyle={styles.input}
          checkboxStyle={styles.checkbox}
          {...restInput}
          {...props}
        />
      </View>
    );
  }

  handleOnChange(val) {
    this.setState({ checked: val });
  }
}
