
import React from 'react';
import StarRating from 'react-native-star-rating';
import { Text, View, Image } from 'react-native';
import Icon from '../../ui/components/Icon';

type Props = {
  // The image of the user who comment
  commentUserImage?: string,
  // The rating of the comment
  rating?: number,
  // The name of the user who comment
  commentUserName?: string,
  // the comment
  comment?: string,

  commentDate?: string,
}

const Comment = (props: Props) => {
  const styles = {
    containerStyle: {
      paddingRight: 2,
      flexDirection: 'row',
      borderRadius: 15,
    },
    nameRateStyle: {
      justifyContent: 'flex-start',
      flexDirection: 'row',
    },
    commentStyle: {
      flexDirection: 'column',
      borderRadius: 10,
      borderWidth: 3,
      borderColor: '#ddd',
      flex: 1,
    },
    textStyle: {
      fontSize: 22,
      fontWeight: 'bold',
      marginRight: 17,
    },
    photo: {
      borderRadius: 50,
      marginLeft: 5,
      marginRight: 5,
    },
    photoImage: {
      height: 60,
      width: 60,
    },
    photoIcon: {
      fontSize: 60,
      height: 60,
    },
    commentTextStyle: {
      fontSize: 18,
    },
  };
  const {
    commentUserImage,
    commentUserName,
    rating,
    comment,
    commentDate,
  } = props;

  const {
    textStyle,
    containerStyle,
    photo,
    photoImage,
    commentStyle,
    photoIcon,
    nameRateStyle,
    starStyle,
    commentTextStyle,
  } = styles;


  return (
    <View>
      <View style={containerStyle}>
        {/*
          {commentUserImage
        ? (
          <Image
            source={{ uri: commentUserImage }}
            style={[photo, photoImage]}
          />
        ) : (
          <Icon
            active
            name="md-person"
            style={[photo, photoIcon]}
          />
        )}
        */}
        <Icon
          active
          name="md-person"
          style={[photo, photoIcon]}
        />
        <View style={commentStyle}>
          <View style={nameRateStyle}>
            <Text style={textStyle}>{commentUserName}</Text>
            <StarRating
              disabled
              selectedStar={() => {}}
              rating={rating}
              halfStar={require('./../../loggedin/images/star-half.png')}
              fullStarColor="black"
              halfStarColor="rgb(255, 245, 0)"
              emptyStarColor="rgb(255, 245, 0)"
              starSize={25}
              style={starStyle}
            />
          </View>
          <Text style={commentTextStyle}>{comment}</Text>
        </View>
      </View>
      <Text> {'  '}{commentDate.getDate()}/{commentDate.getMonth()}/{commentDate.getFullYear()} </Text>
    </View>

  );
};

export default Comment;
