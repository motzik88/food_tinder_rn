
import React from 'react';
import { Text, View } from 'react-native';
import IconButton from './IconButton';

type Props = {
  // The dish name
  headerTitle?: string,
  // The action to call when the button is pressed
  onPress: () => any,
}

const Header = (props: Props) => {
  const styles = {
    viewStyle: {
      // backgroundColor: Theme.PRIMARY,
      flexDirection: 'row',
      // borderRadius: 8,
      borderColor: 'rgb(0,0,0)',
      borderBottomWidth: 1,
      width: '100%',
      alignItems: 'center',
    },
    textStyle: {
      fontSize: 28,
      left: 40,
      color: 'rgb(0,0,0)',
    },
    iconButtonContainer: {
    },
    iconButtonStyle: {
      color: 'rgb(0,0,0)',
    },
  };
  const {
    headerTitle,
    onPress,
  } = props;

  const { textStyle, viewStyle, iconButtonContainer, iconButtonStyle } = styles;
  return (
    <View style={viewStyle}>
      <IconButton
        containerStyle={iconButtonContainer}
        icon="md-arrow-round-forward"
        iconStyle={iconButtonStyle}
        onPress={onPress}
      />
      <Text style={textStyle}>{headerTitle}</Text>
    </View>
  );
};


export default Header;
