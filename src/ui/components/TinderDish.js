import React from 'react';
import { Text, View, ImageBackground, Alert, Image, TouchableOpacity } from 'react-native';
import StarRating from 'react-native-star-rating';
import Card from './card/Card';
import CardSection from './card/CardSection';
import Icon from './Icon';
import renderIf from './../../loggedin/functions/renderIf';

/*
 * We use flow type to validate the Props of the component
 */
type Props = {
  // The dish name
  dishName?: string,
  // The dish description
  dishDescription?: string,
  // if the dish contains things that makes allergies
  allergies?: boolean,
  // if the dish is vegan
  vegan?: boolean,
  // if the dish is vegetarian
  vegetarian?: boolean,
  // The URL of the dish main image
  dishPicture?: string,
  // The number of raters
  raters?: number,
  // The rating of the dish
  rating?: number,
  // the calories of the dishe
  calories?: string,
  // the on prees function
  onPress?: () => any,
}


const TinderDish = (props: Props) => {
  const styles = {
    headerContentStyle: {
      flexDirection: 'column',
      justifyContent: 'space-around',
      flex: 2,
    },

    optionsContentStyle: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
      flex: 2,
    },

    infoContentStyle: {
      flexDirection: 'column',
      justifyContent: 'flex-start',
      width: '100%',
    },
    innerInfoContentStyle: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },

    descriptionTextStyle: {
      fontSize: 18,
      textAlign: 'left',
      alignSelf: 'flex-start',
      // numberOfLines: 1,
      // ellipsizeMode: 'tail',
      lineHeight: 30,
    },

    dishImageStyle: {
      height: 250,
      width: null,
      flex: 1,
      justifyContent: 'flex-end',
      flexDirection: 'column',
    },

    nameContainerStyle: {
      backgroundColor: 'rgba(45, 50, 37, 0.7)',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },

    nameTextStyle: {
      fontSize: 40,
      color: 'rgb(254, 254, 254)',
      fontWeight: 'bold',
      marginLeft: 8,
      marginRight: 8,
      flex: 1,
      textAlign: 'left',
    },

    iconStyle: {
      alignSelf: 'center',
      padding: 5,
      fontSize: 30,
      color: 'rgb(255, 255, 255)',
    },

    imageStyle: {
      alignSelf: 'center',
      padding: 5,
      height: 30,
      width: 30,
      // fontSize: 30,
      // color: 'rgb(255, 255, 255)',
    },

    TouchableOpacityStyle: {
      alignItems: 'center',
      justifyContent: 'center',
    },

    ratingTextStyle: {
      fontSize: 18,
      textAlign: 'center',
      alignSelf: 'center',
    },

  };
  const {
    dishName,
    dishPicture,
    dishDescription,
    allergies,
    vegan,
    vegetarian,
    raters,
    rating,
    calories,
    onPress,
  } = props;

  const {
    descriptionTextStyle,
    headerContentStyle,
    nameContainerStyle,
    nameTextStyle,
    infoContentStyle,
    optionsContentStyle,
    dishImageStyle,
    innerInfoContentStyle,
    iconStyle,
    imageStyle,
    TouchableOpacityStyle,
    ratingTextStyle,
  } = styles;

  return (
    <Card>
      <CardSection>
        {/* This section is for the picture of the dish */}
        <ImageBackground
          source={{ uri: dishPicture }}
          style={dishImageStyle}>
          { /* onPress={() => onPress} */ }

          <View style={nameContainerStyle}>
            {/* This section is for the name of the dish */}
            <Text style={nameTextStyle}>{dishName}</Text>
            {/* This section is for the allergies tags
                CHANGE THIS
              */}
            {renderIf(
              allergies,
              <TouchableOpacity style={TouchableOpacityStyle} onPress={() => Alert.alert('מידע על הסמל', 'המנה מכילה אלרגנים')}>
                <Icon name="md-warning" style={iconStyle} />
              </TouchableOpacity>,
            )}
            {renderIf(
              vegan,
              <TouchableOpacity style={TouchableOpacityStyle} onPress={() => Alert.alert('מידע על הסמל', 'המנה טבעונית')}>
                <Image source={require('./../../loggedin/images/vegan.png')} style={imageStyle} onPress={() => Alert.alert('מידע על הסמל', 'המנה טבעונית')} />
              </TouchableOpacity>,
            )}
            {renderIf(
              vegetarian,
              <TouchableOpacity style={TouchableOpacityStyle} onPress={() => Alert.alert('מידע על הסמל', 'המנה צמחונית')}>
                <Icon name="md-leaf" style={iconStyle} />
              </TouchableOpacity>,
            )}
          </View>
        </ImageBackground>
      </CardSection>

      <CardSection>
        {/* This section is for the details of the dish */}
        <View style={infoContentStyle}>
          <View style={innerInfoContentStyle}>
            <View style={headerContentStyle}>
              {/* This section contains the description and calories of the dish */}
              <Text numberOfLines={1} style={descriptionTextStyle}> ערך קלורי: {calories}</Text>
              <Text numberOfLines={1} style={descriptionTextStyle}> תיאור: {dishDescription}</Text>
            </View>
            <View style={optionsContentStyle}>
              {/* This section contains the rating stars */}
              <StarRating
                disabled
                starSize={25}
                selectedStar={() => {}}
                rating={rating}
                halfStar={require('./../../loggedin/images/star-half.png')}
                fullStarColor="black"
                halfStarColor="rgb(255, 245, 0)"
                // emptyStarColor="rgb(255, 245, 0)"
              />
              <Text style={ratingTextStyle}>({raters})</Text>
            </View>
          </View>
        </View>
      </CardSection>
    </Card>
  );
};

export default TinderDish;
