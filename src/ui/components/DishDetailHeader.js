import React from 'react';
import { Text, View, ImageBackground } from 'react-native';
import Card from './card/Card';
import CardSection from './card/CardSection';
import Header from './Header';
import Icon from './Icon';

/*
 * We use flow type to validate the Props of the component
 */

 type Props = {
   // The dish
   dish: () => any,
   // The action to call when the button is pressed
   onPress: () => any,
 }

const DishDetailHeader = (props: Props) => {
  const styles = {
    containerStyle: {
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'center',
      width: '100%',
      direction: 'rtl',
    },
    dishImageStyle: {
      height: 250,
      width: '100%',
      justifyContent: 'flex-end',
      flexDirection: 'column',
      top: 1,
    },
    icon: {
      fontWeight: 'bold',
      alignItems: 'center',
      textAlign: 'left',
      fontSize: 40,
      color: 'rgb(254, 254, 254)',
      direction: 'rtl',
    },
    rateTextStyle: {
      alignItems: 'center',
      fontSize: 30,
      color: 'rgb(254, 254, 254)',
      fontWeight: 'bold',
      marginLeft: 8,
      marginRight: 8,
      textAlign: 'left',
      direction: 'rtl',
    },
    nameTextStyle: {
      alignItems: 'center',
      fontSize: 30,
      color: 'rgb(254, 254, 254)',
      fontWeight: 'bold',
      marginLeft: 8,
      marginRight: 8,
      textAlign: 'left',
      direction: 'rtl',
    },
    nameContainerStyle: {
      backgroundColor: 'rgba(45, 50, 37, 0.7)',
      flexDirection: 'row',
      justifyContent: 'space-between',
      direction: 'rtl',
    },
  };
  const {
    onPress,
    dish,
  } = props;

  const {
    nameContainerStyle,
    nameTextStyle,
    dishImageStyle,
    infoContentStyle,
    icon,
    containerStyle,
    rateTextStyle,
  } = styles;

  return (
    <View style={containerStyle}>
      <Header
        headerTitle="הרחבה על המנה"
        onPress={onPress}
      />
      <ImageBackground
        source={{ uri: dish.dishPicture }}
        style={dishImageStyle}
        borderRadius={8}>

        <View style={nameContainerStyle}>
          {/* This section is for the name of the dish */}
          <Text style={nameTextStyle}>{dish.dishName}</Text>
          <Text style={rateTextStyle}>
            <Icon
              active
              name="md-star-outline"
              style={icon}
            />
            {' '}
            {dish.rating}
          </Text>
        </View>
      </ImageBackground>
    </View>
  );
};

export default DishDetailHeader;
