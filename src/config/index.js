/**
 * @flow
 *
 * This file provides configuration constants for use in the application
 */

// You can find this value in the `android/app/google-service.json` file
// Look for the `client_id` value, which ends in `apps.googleusercontent.com`
export const GOOGLE_SIGN_IN_ANDROID_CLIENT_ID = '654316769441-lr0h795fqr06tebkn3s37k90g7tl7h8a.apps.googleusercontent.com';

// You can find this value in the `ios/GoogleService-Info.plist` file
// Look for the `CLIENT_ID` value, which ends in `apps.googleusercontent.com`
export const GOOGLE_SIGN_IN_IOS_CLIENT_ID = '.apps.googleusercontent.com';
